# OpenCV examples

## detect_by_color.py:

This example detects hands by their color:  
  - First, it divides the image in the background and close-up (hands)  
  - Second, we detect connected pixels and label them.  
  - Finally, we draw rectangles for each one.  

The following images illustrate the process:  

![](data/hands.jpg?raw=true)  
Original image  

![](data/detect_by_color_mask.jpg?raw=true)  
Binary image (Background and Foreground)  
  
![](data/detect_by_color_labeled.jpg?raw=true)  
Labeled Image  
  
![](data/detect_by_color_final.jpg?raw=true)  
Final  

## digits_hofs.py:

This scripts generates the Histogram of Gradients of several digits images and save them in cvs file,
This file can be used for recognise digits using some classification algorithm such as SVM, RandomForest, etc

Sample of images:

![](data/1.png?raw=true)
![](data/2.png?raw=true)
![](data/3.png?raw=true)
![](data/4.png?raw=true)
![](data/5.png?raw=true)

To create HoG of a image (every image is 28x28 pixels), we divide it in 16 subimages of 7x7 pixels, the we generate 
16 values HoF for every subimage and concat them in one HoF, the result es a 16*16 HoF.

The following image represents the Histograms of image which is a 'seven' digit
![](data/hof_7.png?raw=true)

The following image represents the Histograms of image which is a 'two' digit
![](data/hof_2.png?raw=true)

Therefore, every image has 256 values associated to it.

Analysis of this features can be found [here]: <https://www.kaggle.com/emilianito/digitsocr>

## sudoku_detect.py:

Given a image of Sudoku game, the idea is detecting and collecting the numbers and positions of them and return a matrix in such a way that, later, to be able to resolve the game.

I did the following steps:

- Delete noise and binarize the original image.
- Find the corners of the main square
- Improve the perspective
- Clean the grid
- Detect digits positions

The following images show the proccess:

![](data/sudoku1.jpg?raw=true)  
![](data/sudoku2.jpg?raw=true)  
![](data/sudoku3.jpg?raw=true) 
![](data/sudoku4.jpg?raw=true) 
![](data/sudoku5.jpg?raw=true) 
