#!/usr/bin/env python

'''

Create and save the Histograms of Gradients of images datasources (MNIST DATABASE)
and save them in a csv dataset to be analized later.

The MNIST database of handwritten digits, has a training set of 60,000 examples, and a test set of 10,000 examples. It is a subset of a larger set available from NIST. http://yann.lecun.com/exdb/mnist/

Usage:
   digits_hofs.py [out.csv]

'''

import sys
import numpy as np
from numpy.linalg import norm
import cv2
import math
import csv

# For show and save images

# import matplotlib
# import matplotlib.pyplot as plt
# matplotlib.use("TkAgg")


# Here I have 10000 samples png images

DIGITS_BASEDIR = "/home/emiliano/Downloads/digits/"

# Labels File

LABELS = "data/labels"

def preprocess_hog(img):

    gx = cv2.Sobel(img, cv2.CV_32F, 1, 0)
    gy = cv2.Sobel(img, cv2.CV_32F, 0, 1)
    mag, ang = cv2.cartToPolar(gx, gy)
    bin_n = 16
    bin = np.int32(bin_n*ang/(2*np.pi))

    bin_cells = (bin[:7,:7], bin[7:14,:7], bin[14:21,:7], bin[21:28,:7],  
                bin[:7,7:14], bin[7:14,7:14], bin[14:21,7:14], bin[21:28,7:14], 
                bin[:7,14:21], bin[7:14,14:21], bin[14:21,14:21], bin[21:28,14:21], 
                bin[:7,21:28], bin[7:14,21:28], bin[14:21,21:28], bin[21:28,21:28])

    mag_cells = (mag[:7,:7], mag[7:14,:7], mag[14:21,:7], mag[21:28,:7], 
                mag[:7,7:14], mag[7:14,7:14], mag[14:21,7:14], mag[21:28,7:14], 
                mag[:7,14:21], mag[7:14,14:21], mag[14:21,14:21], mag[21:28,14:21], 
                mag[:7,21:28], mag[7:14,21:28], mag[14:21,21:28], mag[21:28,21:28])

    hists = [np.bincount(b.ravel(), m.ravel(), bin_n) for b, m in zip(bin_cells, mag_cells)]

    #For show and save histograms
    #p = 0
    #x = np.arange(0,16,1)
    #fig, ax = plt.subplots(nrows=4, ncols=4)
    #for row in ax:
    #    for col in row:
    #        col.bar(x, hists[p])
    #        col.axes.get_xaxis().set_ticks([])
    #        col.axes.get_yaxis().set_ticks([])
    #        p += 1
    #fig.savefig('hof.png')
    #plt.show()

    hist = np.hstack(hists)

    # Transform to Hellinger Kernel

    eps = 1e-7
    hist /= hist.sum() + eps
    hist = np.sqrt(hist)
    hist /= norm(hist) + eps

    return hist

if __name__ == "__main__":

    outfile = "/tmp/out.csv"
    if len(sys.argv) >= 2:
        outfile = sys.argv[1]

    with open(LABELS, 'r') as labels_file, open(outfile, 'w') as csvfile:

        features = [ "c%s" % n for n in range(0,256) ]
        features.append("label")

        writer = csv.DictWriter(csvfile, fieldnames=features)
        writer.writeheader()

        i = 1
        for label in labels_file:

            label = label.strip()

            img_path = "%s%d.png" % (DIGITS_BASEDIR, i)

            img_orig = cv2.imread(img_path, 0)

            # First, we rotate the digit just in case.

            m = cv2.moments(img_orig)
            if abs(m['mu02']) >= 1e-2:
                skew = m['mu11']/m['mu02']
                M = np.float32([[1, skew, -0.5*28*skew], [0, 1, 0]])
                img_orig = cv2.warpAffine(img_orig.copy(), M, (28,28), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR)

            # Get Histogram of Gradients

            hist = preprocess_hog(img_orig)

            row = dict(list(zip(features,hist)))
            row["label"] = int(label)
            writer.writerow(row)
            i += 1
