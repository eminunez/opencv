import cv2
import numpy as np

GRID_SIZE = 342

class SudokuDetect:

    def __init__(self, image):
        self.sudoku = cv2.imread(image)

    def show_corners(self, img_, pts):
        img = img_.copy() 
        if(len(img.shape)==2):
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        cv2.circle(img, tuple(pts[0]), 4, (0,255,0), -1)
        cv2.circle(img, tuple(pts[1]), 4, (0,255,0), -1)
        cv2.circle(img, tuple(pts[2]), 4, (0,255,0), -1)
        cv2.circle(img, tuple(pts[3]), 4, (0,255,0), -1)
        cv2.imshow('corners', img)
        cv2.imwrite("data/sudoku4.jpg", img)

    def show_digits(self, img_, digits):
        img = img_.copy()
        if(len(img.shape)==2):
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        for digit in digits:
            p1 = digit[0]
            p2 = tuple(map(sum,list(zip(p1,digit[1]))))
            cv2.rectangle(img, p1, p2, (0,0,255),2) 
        cv2.imshow('digits', img)
        cv2.imwrite("data/sudoku5.jpg", img)

    def show(self):
        cv2.imshow('sudoku', self.sudoku)
        cv2.imwrite("data/sudoku1.jpg", self.sudoku)
        cv2.imshow('bin', self.bin)
        cv2.imwrite("data/sudoku2.jpg", self.bin)
        cv2.imshow('grid', self.grid)
        cv2.imwrite("data/sudoku3.jpg", self.grid)
        self.show_corners(self.sudoku, self.corners)
        self.show_digits(self.grid, self.digits)

    def wait(self):
        cv2.waitKey(0)

    def binarize(self):

        gray = cv2.cvtColor(self.sudoku, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray,(9,9),0)
        self.bin = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)   

    def search_max_grid(self):

        _, contours, _ = cv2.findContours(self.bin, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        biggest = None
        max_area = 0
        pts = []
        for contour in contours:    
            area = cv2.contourArea(contour)
            if area > 100:
                peri = cv2.arcLength(contour,True)
                approx = cv2.approxPolyDP(contour,0.02*peri,True)
                if area > max_area and len(approx)==4:
                    biggest = approx
                    max_area = area
        
        if len(biggest) > 0:

            pts = [ tuple(p[0]) for p in biggest ]
            pts = sorted(pts, key=lambda x: x[1])
            pts = sorted(pts[0:2], key=lambda x: x[0]) + sorted(pts[2:4], key=lambda x: x[0])

        return pts

    def persepective(self, pts):

        h = np.array([ [0,0],[GRID_SIZE-1,0],[0,GRID_SIZE-1],[GRID_SIZE-1,GRID_SIZE-1] ],np.float32)
        M = cv2.getPerspectiveTransform(np.float32(pts),h)
        self.grid = cv2.warpPerspective(self.bin, M, (GRID_SIZE,GRID_SIZE))

    def clean_grid(self):

        _, self.grid = cv2.threshold(self.grid, 127, 255, cv2.THRESH_BINARY)
        cv2.floodFill(self.grid, None, (0, 0), (0, 0, 0))
        kernel = np.ones((1,1), np.uint8)
        self.grid = cv2.erode(self.grid, kernel, iterations=2)    

    def detect_digits(self):
        _, contours, hierarchy = cv2.findContours(self.grid, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        digits = []
        for contour in contours:
            x,y,w,h = cv2.boundingRect(contour)
            if (h/w) > 1.2 and (h/w) < 2.6 and h > 10 and h < 30:
                digits.append(((x,y),(w,h)))
        return digits

    def scann(self):

        self.binarize()
        self.corners = self.search_max_grid()

        if len(self.corners) > 0:
            self.persepective(self.corners)
            self.clean_grid()
            self.digits = self.detect_digits()        
        else:
            print("Grid not found")

if __name__ == "__main__":

    sudo = SudokuDetect("data/sudoku-original.jpg")
    sudo.scann()
    sudo.show()
    sudo.wait()