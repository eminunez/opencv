#!/usr/bin/env python

'''

Object detection by color

Usage:
   detect_by_color.py

'''

import cv2
import numpy as np

IMG = "data/hands.jpg"

## Load Image

img = cv2.imread(IMG, 1)
cv2.imshow('orig', img)

## Convert BGR to HSV

hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

## Filter by range of color in HSV

lower_blue = np.array([0, 25, 25])
upper_blue = np.array([255, 255, 255])
mask = cv2.inRange(hsv, lower_blue, upper_blue)
cv2.imshow("mask", mask)
cv2.imwrite("data/detect_by_color_mask.jpg", mask)

## Label Connected pixeles

ret, labels = cv2.connectedComponents(mask, connectivity=8)
label_hue = np.uint8(179*labels/np.max(labels))
blank_ch = 255*np.ones_like(label_hue)
labeled_img = cv2.merge([label_hue, blank_ch, blank_ch])
labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)
labeled_img[label_hue==0] = 0
cv2.imshow("labeled", labeled_img)
cv2.imwrite("data/detect_by_color_labeled.jpg", labeled_img)

## Locate Windows

windows = {}
for i in range(1, ret):
    windows[i] = {"x1":labels.shape[0], "y1":labels.shape[1], "x2":0, "y2":0}

for row in range(0,len(labels)):
    for col in range(0,len(labels[row])):
        px = labels[row][col]
        if px > 0:
            if col < windows[px]["x1"]:
                windows[px]["x1"] = col
            if col > windows[px]["x2"]:
                windows[px]["x2"] = col
            if row < windows[px]["y1"]:
                windows[px]["y1"] = row
            if row > windows[px]["y2"]:
                windows[px]["y2"] = row

# Draw Rectangles

for i in windows:
    w = windows[i]
    img = cv2.rectangle(img, (w["x1"],w["y1"]), (w["x2"],w["y2"]), (0,255,0), 2)

cv2.imshow("final", img)
cv2.imwrite("data/detect_by_color_final.jpg", img)

cv2.waitKey(0)
cv2.destroyAllWindows()
